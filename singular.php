<?php

get_header();

?>
<!-- main start -->
<main>
	<div id="categories-section">
		<div class="container">
			<div class="singular">
				<header>
					<h1>
						<?php

						the_title();

						?>
					</h1>
					<?php

					if ( has_excerpt() )
					{
						the_excerpt();
					}

					?>
				</header>
				<div>
					<?php

					the_content();

					?>
				</div>
				<footer>
				</footer>
			</div>


		</div>
	</div>
</main>


<?php

get_footer();

