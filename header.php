<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	
	wp_head();

	// never code like this, kids. bootstrap leads to the dark side.

	?>
  <title>IQ Trivia</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/base.css">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/header.css">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/categories.css">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/footer.css">

  <?php

  /*
  scripts not needed
	
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/popper.min.js" defer></script>
	<!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-3.4.1.slim.min.js" defer></script> -->
  */

  ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" defer></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js" defer></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

</head>
<body>
<header class="bg-light siteHeader">
  <div class="container">
	<nav class="navbar navbar-expand-lg navbar-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarToggler">
			<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logoIQ.jpg" width="100"></a>
			<ul class="navbar-nav mr-auto"></ul>
			<ul class="navbar-nav ml-0 ml-0 mt-2 mt-lg-0">
		  	<?php

		  	if ( is_user_logged_in() )
		  	{
		  		?>
				  <li class="nav-item">
				  		<a class="nav-link" href="<?php echo get_author_posts_url( get_current_user_id() ); ?>" >My Profile</a>
				  </li>
				  <li class="nav-item">
				  		<a class="nav-link logout-link" href="<?php echo wp_logout_url( get_home_url() ); ?>"><i class="bi bi-box-arrow-right"></i>Logout</a>
				  </li>
		  		<?php
		  	}
		  	else
		  	{
		  		?>
		  		<a class="nav-link logout-link" href="<?php echo get_site_url(); ?>/login"><i class="bi bi-box-arrow-right"></i>Log in</a>
		  		<?php
		  	}

		  	?>
			</ul>
	  </div>
	</nav>
  </div>
</header>
