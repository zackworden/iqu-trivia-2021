<?php

get_header();

?>
<main>
	<?php

	get_template_part('partials/trivia/triviaQuestion');

	get_template_part('partials/trivia/triviaModal');

	?>
</main>
<?php

$query = new WP_Query( TriviaUtility::getTriviaQueryArgs( get_queried_object()->slug, get_current_user_id() ) );

if ( $query->have_posts() )
{
	while ( $query->have_posts() )
	{
		$query->the_post();

		get_template_part('partials/trivia/triviaAjaxObject');
	}

	wp_reset_postdata();
}
else
{
	$query = new WP_Query( TriviaUtility::getTriviaQueryArgsNoDedupe( get_queried_object()->slug, get_current_user_id() ) );

	if ( $query->have_posts() )
	{
		while ( $query->have_posts() )
		{
			$query->the_post();

			get_template_part('partials/trivia/triviaAjaxObject');
		}

		wp_reset_postdata();
	}
	else
	{
		$query = new WP_Query( TriviaUtility::getTriviaQueryArgsNoDedupeNoDifficulty( get_queried_object()->slug ) );
		
		if ( $query->have_posts() )
		{
			while ( $query->have_posts() )
			{
				$query->the_post();

				get_template_part('partials/trivia/triviaAjaxObject');
			}

			wp_reset_postdata();
		}
		else
		{
			// super fallback - maybe just a random trivia question?
		}
	}
}

get_template_part('partials/trivia/triviaAjaxMainScriptInit');

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/questions.css">
<?php

get_footer();

