// var multiQuizHandler = {

// 	homeUrl : '',
// 	ajaxUrl : '',
// 	answerNonce : '',
// 	userId : -1,
// 	quizData : {},

// 	doModalResponse : function( isUserCorrect ) {

// 		var modalHeader = document.querySelector('.modal-header');
// 		var modalTitle = document.querySelector('.modal-title');
// 		var modalSubtitle = document.querySelector('.modal-body h3');

// 		if ( isUserCorrect )
// 		{
// 			modalHeader.classList.add('modal-header','modal-success');
// 			modalHeader.classList.remove('modal-danger');
// 			modalTitle.innerText = 'Congratulations!';
// 			modalSubtitle.classList.add('text-success');
// 			modalSubtitle.classList.remove('text-danger');
// 			modalSubtitle.innerText = 'You have won a bracelet!';
// 		}
// 		else
// 		{
// 			modalHeader.classList.add('modal-header','modal-danger');
// 			modalHeader.classList.remove('modal-success');
// 			modalTitle.innerText = 'Sorry!';
// 			modalSubtitle.classList.add('text-danger');
// 			modalSubtitle.classList.remove('text-success');
// 			modalSubtitle.innerText = 'You have lost a bracelet!';
// 		}

// 		$('#questionResult').modal('show');

// 		$('#questionResult').on(
// 			'hidden.bs.modal', 
// 			function() {
// 				window.location.href = multiQuizHandler.homeUrl;
// 			}
// 		);
// 	},

// 	addFormListener : function() {

// 		event.preventDefault();

// 		var quizSubmitElem = document.querySelector('#answer-submit');

// 		quizSubmitElem.addEventListener(
// 			'click',
// 			function ( event ) {

// 				// TODO - add validation that something has actually been picked

// 				var quizFormElem = document.querySelector('#quizForm');

// 				var isUserCorrect = multiQuizHandler.evalAnswer( quizFormElem['customRadio'].value );

// 				multiQuizHandler.doModalResponse( isUserCorrect );
// 				multiQuizHandler.recordAnswer( isUserCorrect );
// 			}
// 		);
// 	},

// 	populateQuestion : function() {

// 		var questionElem = document.querySelector('#question1');
// 		questionElem.innerHTML = multiQuizHandler.quizData['question'];
// 	},

// 	populateForm : function() {

// 		var quizFormElem = document.querySelector('#quizForm');
// 		var counter = 0;
// 		var numOf = multiQuizHandler.quizData.answers.length;
// 		var answerElem;

// 		for ( counter = 0; counter < numOf; counter ++ )
// 		{
// 			answerElem = document.createElement('div');
// 			answerElem.classList.add('custom-control', 'custom-radio');
// 			answerElem.innerHTML = '<input type="radio" id="option' + (counter + 1) + '" value="' + encodeURI( multiQuizHandler.quizData.answers[ counter ] ) + '" name="customRadio" class="custom-control-input" ><label class="custom-control-label" for="option' + (counter + 1) + '">' + multiQuizHandler.quizData.answers[ counter ] + '</label>';

// 			quizFormElem.prepend( answerElem );
// 		}

// 	},

// 	evalAnswer : function( submittedAnswer ) {

// 		var encodedCorrectAnswer = encodeURI( multiQuizHandler.quizData['correct'] );

// 		return (encodedCorrectAnswer == submittedAnswer);
// 	},

// 	recordAnswer : function( isCorrect ) {

//         var postData = {
//           'user_id'			: multiQuizHandler.userId,
//           'is_correct'		: isCorrect,
//           'answer_nonce'	: multiQuizHandler.answerNonce,
//           'action'      	: 'doTriviaAnswer'
//         };

//         jQuery.ajax(
//           {
//             'type'    : 'POST',
//             'url'     : multiQuizHandler.ajaxUrl,
//             'data'    : postData
//           }
//         );
// 	},

// 	init : function( quizData ) {
// 		multiQuizHandler.quizData = quizData;
// 	}

// };