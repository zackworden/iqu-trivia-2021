<?php

require_once 'trivia.php';

// handle ajax user registration
function iqu_doMemberSignup()
{
	$requestResponse = [];

	if (
		!empty( $_POST['loginNonce'] ) &&
		wp_verify_nonce( $_POST['loginNonce'], 'register_nonce' ) &&
		!empty( $_POST['email'] ) &&
		!empty( $_POST['name'] ) &&
		!empty( $_POST['pass'] ) 
	)
	{
		$memberEmail = sanitize_email( $_POST['email'] );
		$memberName = sanitize_text_field( $_POST['name'] );
		$memberPass = $_POST['pass'];
		$userName = $memberName;

		// generate unique username
		$counter = 0;

		while ( 
			username_exists( $userName ) != FALSE &&
			$counter < 9999
		)
		{
			$counter ++;
			$userName .= $counter;
		}

		$userResponse = wp_create_user( $userName, $memberPass, $memberEmail );

		// error handling
		if ( is_a( $userResponse, 'WP_Error') )
		{
			$requestResponse['success'] = FALSE;

			if ( !empty( $userResponse->errors ) )
			{
				if ( !empty( $userResponse->errors['existing_user_login'] ) )
				{
					$requestResponse['description'] = 'username_exists';
				}
				else if ( !empty( $userResponse->errors['existing_user_email'] ) )
				{
					$requestResponse['description'] = 'email_exists';
				}
				else if ( !empty( $userResponse->errors['empty_user_login'] ) )
				{
					$requestResponse['description'] = 'no_username';
				}
			}
		}
		else if ( is_int( $userResponse ) )
		{
			$requestResponse['success'] = TRUE;
			$requestResponse['success'] = 'ok1';

			$requestResponse['description'] = [
				'field_612013acaf8d5',
				$memberName,
				'user_' . $userResponse
			];

			wp_set_auth_cookie( $userResponse, TRUE );
		}
	}

	echo json_encode( $requestResponse );
	exit;
}
add_action('wp_ajax_nopriv_doMemberSignup', 'iqu_doMemberSignup');
add_action('wp_ajax_doMemberSignup', 'iqu_doMemberSignup');


function iqu_doMemberProfileUpdate()
{
	$requestResponse = [];
	$requestResponse['success'] = FALSE;

	if (
		!empty( $_POST['memberupdatenonce'] ) &&
		is_user_logged_in() == TRUE &&
		!empty( wp_verify_nonce( $_POST['memberupdatenonce'], 'update_nonce') )
	)
	{
		// first name
		if ( isset( $_POST['firstname'] ) )
		{
			update_user_meta( get_current_user_id(), 'first_name', strval( sanitize_text_field( $_POST['firstname'] ) ) );
		}

		// last name
		if ( isset( $_POST['lastname'] ) )
		{
			update_user_meta( get_current_user_id(), 'last_name', strval( sanitize_text_field( $_POST['lastname'] ) ) );
		}

		// bio
		if ( isset( $_POST['bio'] ) )
		{
			update_field( 'field_61256c01ebfd6', strval( sanitize_textarea_field( $_POST['bio'] ) ), 'user_' . get_current_user_id() );
		}

		// hometown
		if ( isset( $_POST['hometown'] ) )
		{
			update_field( 'field_61256be4cb3fb', strval( sanitize_text_field( $_POST['hometown'] ) ), 'user_' . get_current_user_id() );
		}

		// trailname
		if ( isset( $_POST['trailname'] ) )
		{
			$theKey = 'field_612013acaf8d5';
			$theVal = sanitize_text_field( $_POST['trailname'] );
			$thePost = 'user_' . get_current_user_id();
			$res = update_field( $theKey, $theVal, $thePost );
		}

		// user photo, if available
		if ( isset( $_FILES['photo'] ) ) 
		{
			// validate file format
			if (
				in_array($_FILES['photo']['type'], ['image/jpeg','image/png','image/jpg'])
			)
			{
				$imageData = [
					'post_title' 		=> 'Profile Photo: ' . get_current_user_id(),
					'post_excerpt'		=> '',
					'post_content'		=> 'Profile Photo for user ' . get_current_user_id(),
				];

				$attachId = media_handle_upload(
					'photo',
					0,
					$imageData
				);

				$isSuccess = MemberUtility::upsertProfilePic( get_current_user_id(), $attachId );
			}
		}

		// interests / hobbies
		if ( isset( $_POST['interests'] ) ) 
		{
			update_field( 'field_617ef7413834b', strval( sanitize_text_field( $_POST['interests'] ) ), 'user_' . get_current_user_id() );
		}

		// birthday
		if ( isset( $_POST['birthday'] ) ) 
		{
			// field_617ef75a3834c
		}

		// gender
		if ( isset( $_POST['gender'] ) )
		{
			update_field( 'field_617ef78b3834d', strval( sanitize_text_field( $_POST['gender'] ) ), 'user_' . get_current_user_id() );
		}

		$requestResponse = [];
		$requestResponse['success'] = TRUE;
		$requestResponse['message'] = 'Success';
	}

	echo json_encode( $requestResponse );
	exit;
}
add_action('wp_ajax_doMemberProfileUpdate', 'iqu_doMemberProfileUpdate');


// handle ajax user login 
function iqu_doMemberLogin()
{
	$requestResponse = [];
	$requestResponse['success'] = FALSE;

	if (
		!empty( $_POST['email'] ) &&
		!empty( $_POST['pass'] ) &&
		!empty( $_POST['loginNonce'] )
	)
	{
		if ( wp_verify_nonce( $_POST['loginNonce'], 'login_nonce') )
		{
			$email = $_POST['email'];
			$pass = $_POST['pass'];

			$wpUser = get_user_by( 'email', $email );
			
			if ( 
				$wpUser &&
				!empty( $wpUser->roles ) 
			)
			{
				$loginResponse = wp_signon(
					[
						'user_login'		=> $email,
						'user_password'		=> $pass,
						'remember'			=> TRUE,
					]
				);

				if ( $loginResponse instanceof WP_User )
				{
					$requestResponse['success'] = TRUE;
					$requestResponse['message'] = 'Success';
				}
				else if ( $loginResponse instanceof WP_Error )
				{
					if ( 
						isset( $loginResponse->errors['invalid_email'] )  ||
						isset( $loginResponse->errors['invalid_username'] )  ||
						isset( $loginResponse->errors['incorrect_password'] ) 
					)
					{
						$requestResponse['success'] = FALSE;
						$requestResponse['message'] = 'Invalid login credentials.';
						$requestResponse['message'] = 'Login user / pass not found. Please verify and try again.';
					}
					else
					{
						$requestResponse['message'] = 'Unexplained login issue encountered.';
					}
				}
			}
			else
			{
				$requestResponse['message'] = 'Email and/or password not found!';
			}
		}
		else
		{
			$requestResponse['message'] = 'Login attempt expired. Please refresh and try again.';
		}
	}
	else
	{
		$requestResponse['message'] = 'Required fields missing';
	}

	echo json_encode( $requestResponse );
	exit;
}
add_action('wp_ajax_nopriv_doMemberLogin', 'iqu_doMemberLogin');
add_action('wp_ajax_doMemberLogin', 'iqu_doMemberLogin');


// handle ajax trivia answer
function iqu_doTriviaAnswer()
{
	$requestResponse = [];
	$requestResponse['success'] = FALSE;

	if (
		is_user_logged_in() &&
		isset( $_POST['post_id'] ) &&
		isset( $_POST['is_correct'] ) &&
		!empty( $_POST['answer_nonce'] ) &&
		wp_verify_nonce( $_POST['answer_nonce'], 'answer_nonce')
	)
	{
		$userId = get_current_user_id();
		$currentScore = get_field( 'user_bracelets', 'user_' . $userId );
		$currentScore ++;
		update_field( 'user_bracelets', $currentScore, 'user_' . $userId );

		// record completed questions
		MemberUtility::setQuestionAsCompleted( intval($_POST['post_id']), get_current_user_id() );

		$requestResponse['success'] = TRUE;
	}

	echo json_encode( $requestResponse );
	exit;
}
add_action('wp_ajax_doTriviaAnswer', 'iqu_doTriviaAnswer');
add_action('wp_ajax_nopriv_doTriviaAnswer', 'iqu_doTriviaAnswer');
