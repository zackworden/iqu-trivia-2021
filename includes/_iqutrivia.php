<?php

// enqueue scripts and styles somewhat properly
function iqu_enqueueScriptsAndStyles()
{
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_style( 'uikit', get_stylesheet_directory_uri() . '/css/uikit.css' );
    wp_enqueue_script( 'uikit', get_stylesheet_directory_uri() . '/js/uikit.js' );
}
add_action( 'wp_enqueue_scripts', 'iqu_enqueueScriptsAndStyles' );

// updates the URL of the login screen logo
function iqu_updateLoginUrl()
{
	return home_url();
}
add_filter( 'login_headerurl', 'iqu_updateLoginUrl' );

// updates title text of logo, from "Powered by WordPress" to site title
function iqu_updateLogoTitle()
{
	return 'IQU Trivia';
}
add_filter( 'login_headertitle', 'iqu_updateLogoTitle' );



// adds custom styling / overrides the generic WP login. this has to be here.
function iqu_customizeLoginScreen()
{
	?>
	<style type="text/css">
		body.login
		{
			background-color: #FFF;
		}
		#login h1 a, 
		.login h1 a 
		{
			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/logoIQ.jpg');
			height: 306px;
			width: 426px;
			background-size: 426px 306px;
			background-repeat: no-repeat;
			padding-bottom: 30px;
		}
		#login
		{
			padding:  2rem 1rem !important;
			box-sizing: border-box;
			width: unset !important;
			max-width: 60rem;
			color: #EEE;
		}
		body.login label
		{
			font-size: 1rem;
		}
		body.login form input
		{
			border:  2px solid #25408F;
		}
		body.login form input:hover,
		body.login form input:focus,
		body.login form input:active
		{
			border-color: #25408F;	
		}

		

		.login #backtoblog a,
		.login #nav a
		{
			font-size: 1.2rem !important;
			color: #333 !important;
		}
		.login #backtoblog a:hover,
		.login #backtoblog a:focus,
		.login #nav a:hover,
		.login #nav a:focus
		{
			text-decoration: underline !important;
		}
		#lostpasswordform,
		#loginform
		{
			background-color: #FFF;
			border: 2px solid #25408F;
			border-radius: .25rem;
			margin: 0 20px;
		}
		#lostpasswordform label,
		#loginform label
		{
			color: #333 !important;
		}
		#login_error,
		.message
		{
			border-left: unset !important;
			text-align: center !important;
			font-size: 1.2rem;
			background-color: rgba(0,0,0, 0) !important;
			color: #333 !important;
			color: #5f6673 !important;
			line-height:1.4rem;
		}
		#wp-submit
		{
			display: inline-block !important;
			margin-bottom: .75rem !important;
			background-color: #FFF !important;
			color: #25408F !important;
			border:2px solid #25408F !important;
			border-radius: .15rem !important;
			padding: .75rem 1.25rem !important;
			font-weight: bold !important;
			cursor: pointer !important;
			height: unset !important;
			line-height: unset !important;
			vertical-align: middle !important;
			text-shadow: unset !important;
			font-weight: 700 !important;
			text-transform: uppercase !important;
			box-shadow: unset !important;
			transition:box-shadow .2s, background-color .2s;
		}
		#wp-submit:hover,
		#wp-submit:focus
		{
			background-color: #25408F !important;
			color: #FFFFFF !important;
			box-shadow: 0 0 1rem #25408F;
		}
		#wp-submit:active
		{
			box-shadow: 0 0 1rem green inset;
		}
	</style>
	<?php		
}
add_action( 'login_enqueue_scripts', 'iqu_customizeLoginScreen' );


function iqu_doMembersOnlyRedirect()
{
	if ( !is_user_logged_in() )
	{
		if ( 
			is_single() ||
			is_category()
		)
		{
			wp_redirect( get_home_url() );
			exit;
		}
	}
}
add_action('template_redirect', 'iqu_doMembersOnlyRedirect');



function iqu_registerThumbnailSizes()
{
    add_image_size( 'categoryIcon', 400, 300, ['center', 'center'] );
    add_image_size( 'profilePhoto', 500, 500, ['center', 'center'] );
}
add_action( 'after_setup_theme', 'iqu_registerThumbnailSizes' );



// TODO - organize me


add_action('admin_init', function ()
{
    // Redirect any user trying to access comments page
    global $pagenow;
    
    if ($pagenow === 'edit-comments.php')
    {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type)
    {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function ()
{
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});





function lwp_2610_custom_author_base() {
    global $wp_rewrite;
    $wp_rewrite->author_base = 'people';
}
add_action( 'init', 'lwp_2610_custom_author_base' );



add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}