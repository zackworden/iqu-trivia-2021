<?php

require_once(ABSPATH . "wp-admin" . '/includes/image.php');
require_once(ABSPATH . "wp-admin" . '/includes/file.php');
require_once(ABSPATH . "wp-admin" . '/includes/media.php');

function init_triviaQuestionPostType()
{
    $singular = 'Trivia';
    $plural = $singular;

    $labelArgs = array (
        'name'                  =>  $plural,
        'singular_name'         =>  $singular,
        'add_new'               =>  'Add New', $singular,
        'add_new_item'          =>  'Add New ' . $singular,
        'edit_item'             =>  'Edit',
        'new_item'              =>  'New',
        'view_item'             =>  'View',
        'search_items'          =>  'Search',
        'not_found'             =>  'No ' . $plural . ' Found',
        'not_found_in_trash'    =>  'No ' . $plural . ' Found in Trash',
        'all_items'             =>  'All ' . $plural,
    );
    
    $args = array(
        'labels'                =>  $labelArgs,
        'description'           =>  '',
        'public'                =>  true,
        'publicly_queryable'    =>  true,
        'exclude_from_search'   =>  false,
        'show_ui'               =>  true,
        'show_in_menu'          =>  true,
        'query_var'             =>  true,
        'rewrite'               =>  array( 'slug' => FALSE, 'with_front' => FALSE, 'feeds' => FALSE, 'pages' => false, 'ep_mask' => EP_PERMALINK ),
        'capability_type'       =>  'post',
        'has_archive'           =>  true,
        'hierarchical'          =>  false,
        'taxonomies'            =>  ['post_tag','category'],
        'menu_position'         =>  8,
        'menu_icon'             =>  'dashicons-share-alt2',
        'supports'              =>  array('title', 'author'),
        'show_in_rest'          =>  true,
    );

    register_post_type( 'trivia', $args );
}
add_action('init','init_triviaQuestionPostType');


function init_triviaDifficultyTaxonomy()
{
	$slottingSingular = 'Difficulty';
	$slottingPlural = $slottingSingular;

	$slottingTaxLabels = array(
			'name'					=>	$slottingPlural,
			'singular_name'         =>	$slottingSingular,
			'menu_name'				=>	$slottingPlural,
			'all_items'				=>	'All ' . $slottingPlural,
			'edit_item'				=>	'Edit ' . $slottingPlural,
			'view_item'				=>	'View ' . $slottingPlural,
			'update_item'           =>	'Update ' . $slottingPlural,
			'add_new_item'          =>	'New ' . $slottingSingular,
			'search_items'          =>	'Search ' . $slottingPlural,
		);
	$slottingTaxArgs = array(
			'label'					=>	'',
			'labels'				=>	$slottingTaxLabels,
			'public'				=>	true,
			'show_ui'				=>	true,
			'show_in_menu'			=>	true,
			'show_in_nav_menus'		=>	false,
			'show_tagcloud'			=>	false,
			'show_in_quick_edit'	=>	true,
			'show_admin_column'		=>	true,//false
			'description'			=>	'',
			'hierarchical'			=>	true,
			'query_var'				=>	true,
			'rewrite'				=>	false,
			'capabilities'			=>	array(
					'manage_terms'	=>	'manage_options',
					'edit_terms'	=>	'manage_options',
					'delete_terms'	=>	'manage_options',
					'assign_terms'	=>	'edit_posts',
				),
		);

	register_taxonomy(
			'difficulty',
			array('trivia'),
			$slottingTaxArgs
		);
}
add_action('init','init_triviaDifficultyTaxonomy');


// adjust difficulty level on login
function iqu_resetDifficultyLevel( $login, $user )
{
	$currentScore = get_field( 'field_61201356895f9', 'user_' . $user->ID );
	$oldDifficulty = get_field( 'field_619027366c805', 'user_' . $user->ID );

	if ( !empty( $currentScore ) ) 
	{
		$currentDifficulty = TriviaUtility::getDifficultyLevel( intval( $currentScore ) );
	}
	else 
	{
		$currentDifficulty = TriviaUtility::getDifficultyLevel( 0 );
	}

	if ( $currentDifficulty !== $oldDifficulty )
	{
		$res = update_field( 'field_619027366c805', $currentDifficulty, 'user_' . $user->ID );
	}
}
add_action('wp_login', 'iqu_resetDifficultyLevel', 10, 2);


abstract class TriviaUtility
{
	static function getDifficultyLevel( int $currentScore = 0 )
	{
		$difficultySlug = 'medium';

		if ( $currentScore > 25 )
		{
			$difficultySlug = 'hard';
		}
		else if ( $currentScore > 10 )
		{
			$difficultySlug = 'medium';
		}
		else
		{
			$difficultySlug = 'easy';
		}

		return $difficultySlug;
	}

	static function getTriviaQueryArgs( $categorySlug, $userId )
	{
		$args = [
			'post_type'				=> 'trivia',
			'posts_per_page'		=> 1,
			'orderby'				=> 'rand',
			'post__not_in'			=> explode( ',', MemberUtility::getCompletedQuestionsForMember( $userId ) ),
			'tax_query'				=> [
				'relation'				=> 'AND',
				[
					'taxonomy'				=> 'category',
					'field'					=> 'slug',
					'terms'					=> $categorySlug,
				],
				[
					'taxonomy'				=> 'difficulty',
					'field'					=> 'slug',
					'terms'					=> MemberUtility::getMemberDifficultyLevel( $userId ),
				],
			],

		];

		return $args;
	}

	static function getTriviaQueryArgsNoDedupe( $categorySlug, $userId )
	{
		$args = [
			'post_type'				=> 'trivia',
			'posts_per_page'		=> 1,
			'orderby'				=> 'rand',
			'tax_query'				=> [
				'relation'				=> 'AND',
				[
					'taxonomy'				=> 'category',
					'field'					=> 'slug',
					'terms'					=> $categorySlug,
				],
				[
					'taxonomy'				=> 'difficulty',
					'field'					=> 'slug',
					'terms'					=> MemberUtility::getMemberDifficultyLevel( $userId ),
				],
			]
		];

		return $args;
	}

	static function getTriviaQueryArgsNoDedupeNoDifficulty( $categorySlug )
	{
		$args = [
			'post_type'				=> 'trivia',
			'posts_per_page'		=> 1,
			'orderby'				=> 'rand',
			'tax_query'				=> [
				'relation'				=> 'AND',
				[
					'taxonomy'				=> 'category',
					'field'					=> 'slug',
					'terms'					=> $categorySlug,
				],
			]
		];

		return $args;
	}
}

abstract class MemberUtility
{
	public static $answeredQuestionsSlug = 'answered_questions';

	static function getMemberDifficultyLevel( $userId )
	{
		$difficultySlug = get_field('user_difficulty', 'user_' . $userId );

		if ( empty($difficultySlug) )
		{
			$difficultySlug = 'medium';
		}

		return $difficultySlug;
	}

	static function setQuestionAsCompleted( $postId, $userId )
	{
		$completedQuestionString = self::getCompletedQuestionsForMember( $userId );
		$completedQuestions = explode(',', $completedQuestionString );

		if ( !in_array( $postId, $completedQuestions ) )
		{
			$completedQuestions[] = $postId;
		}

		$newQuestionsString = implode(',', $completedQuestions);

		$newQuestionsString = trim( $newQuestionsString, ', ' );

		update_user_meta( $userId, self::$answeredQuestionsSlug, $newQuestionsString );
	}

	static function getCompletedQuestionsForMember( $userId )
	{
		return get_user_meta( $userId, self::$answeredQuestionsSlug, TRUE );
	}

	static function upsertProfilePic( $userId, $newImageAttachmentId )
	{
		// if existing, remove
		$possibleExistingImage = get_field( 'field_61256bd2cb3fa', 'user_' . $userId );

		if ( !empty($possibleExistingImage) )
		{
			wp_delete_attachment( $possibleExistingImage['ID'],  );
		}

		// custom field for image
		update_field( 'field_61256bd2cb3fa', $newImageAttachmentId, 'user_' . $userId );

		return TRUE;
	}
}










add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Register options page.
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Theme General Settings'),
            'menu_title'    => __('Theme Settings'),
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));
    }
}





/*


function misha_menu_page()
{
	add_menu_page(
		'IQU Trivia Management',
		'Trivia Settings',
		'manage_options',
		'trivia-settings',
		'iqu_doTriviaPage',
		'dashicons-star-half',
		500
	);
}
add_action( 'admin_menu', 'misha_menu_page' );

function iqu_doTriviaPage()
{
	?>
	<h2>
		Trivia Settings
	</h2>
	<?php
}
*/