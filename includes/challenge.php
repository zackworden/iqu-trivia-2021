<?php

function init_challengePostType()
{
    $singular = 'Challenge';
    $plural = $singular . 's';

    $labelArgs = array (
        'name'                  =>  $plural,
        'singular_name'         =>  $singular,
        'add_new'               =>  'Add New', $singular,
        'add_new_item'          =>  'Add New ' . $singular,
        'edit_item'             =>  'Edit',
        'new_item'              =>  'New',
        'view_item'             =>  'View',
        'search_items'          =>  'Search',
        'not_found'             =>  'No ' . $plural . ' Found',
        'not_found_in_trash'    =>  'No ' . $plural . ' Found in Trash',
        'all_items'             =>  'All ' . $plural,
    );
    
    $args = array(
        'labels'                =>  $labelArgs,
        'description'           =>  '',
        'public'                =>  true,
        'publicly_queryable'    =>  true,
        'exclude_from_search'   =>  false,
        'show_ui'               =>  true,
        'show_in_menu'          =>  true,
        'query_var'             =>  true,
        'rewrite'               =>  array( 'slug' => FALSE, 'with_front' => FALSE, 'feeds' => FALSE, 'pages' => false, 'ep_mask' => EP_PERMALINK ),
        'capability_type'       =>  'post',
        'has_archive'           =>  true,
        'hierarchical'          =>  false,
        'taxonomies'            =>  [],
        'menu_position'         =>  8,
        'menu_icon'             =>  'dashicons-share-alt2',
        'supports'              =>  array('title', 'custom-fields'),
        'show_in_rest'          =>  true,
    );

    register_post_type( 'triviachallenge', $args );
}
// add_action('init','init_challengePostType');

