<?php

get_header();

?>
<style>
	.scoreboard
	{

	}
	.scoreboard_item
	{
		margin: .5rem 0;
		padding: .5rem .25rem 1rem .25rem;
		font-size:  22px;
		border-bottom:  1px solid #EEE;
	}
	.scoreboard_item:last-child
	{
		border-bottom: unset;
	}
	.scoreboard_item_rank
	{
		color: #000;
		font-size: 26px;
	}
	.scoreboard_item_name
	{
		color:  black;
		font-size: 26px;
	}
	.scoreboard_item_score
	{
		color:  #444;
	}
</style>
<!-- main start -->
<main>
	<div id="categories-section">
		<div class="container">
			<div class="singular">
				<header>
					<h1>
						Top Scores
					</h1>
				</header>
				<div class="scoreboard">
					<?php

					// TODO - fix me

					global $wpdb;

					$query = "
						SELECT 
							ID
						FROM
							`wp_users` U
						LEFT JOIN
							`wp_usermeta` UM
						ON
							U.ID = UM.user_id
						WHERE
							UM.meta_key = \"user_bracelets\"
						ORDER BY
							UM.meta_value
						DESC;

					";

					$userIds = $wpdb->get_results( $query, ARRAY_A );

					$rank = 1;

					if ( !empty( $userIds ) )
					{
						foreach ( $userIds as $thisUserId )
						{
							if ( $thisUserId['ID'] != 0 )
							{
								$numOfBracelets = get_field('user_bracelets', 'user_' . $thisUserId['ID'] );
								$name = get_field('user_trailname', 'user_' . $thisUserId['ID'] );

								if ( empty( $name ) )
								{
									$name = 'Anonymous Hiker Trash';
								}

								?>
								<div class="scoreboard_item">
									<span class="scoreboard_item_rank"><?php echo $rank; ?></span> - <span class="scoreboard_item_name"><?php echo $name; ?></span>. <br />
									<span class="scoreboard_item_score">Score: <?php echo $numOfBracelets; ?></span>
								</div>
								<?php

								$rank ++;
							}
						}
					}
					else
					{
						?>
						No users found
						<?php
					}

					?>
				</div>
				<footer>
				</footer>
			</div>


		</div>
	</div>
</main>


<?php

get_footer();

