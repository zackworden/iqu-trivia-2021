<?php

get_header();

?>
<main>
	<?php

	if ( is_user_logged_in() )
	{
		get_template_part('partials/frontpage/frontpage','loggedIn');
	}
	else
	{
		get_template_part('partials/shared/form','login');
	}

	?>
</main>
<?php

get_footer();

