<?php

get_header();

?>
<main>
	<?php

	get_template_part('partials/trivia/triviaQuestion');

	get_template_part('partials/trivia/triviaModal');
	
	?>
</main>
<?php

if ( have_posts() )
{
	while ( have_posts() )
	{
		the_post();

		get_template_part('partials/trivia/triviaAjaxObject');
	}
}

get_template_part('partials/trivia/triviaAjaxMainScriptInit');

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/questions.css">
<?php

get_footer();

