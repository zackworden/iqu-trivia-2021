<?php

get_header();


$thePerson = get_queried_object();

$numOfBracelets = get_field( 'user_bracelets', 'user_' . $thePerson->ID );
$trailName = get_field( 'user_trailname', 'user_' . $thePerson->ID );
$photo = get_field( 'user_photo', 'user_' . $thePerson->ID );
$hometown = get_field( 'user_hometown', 'user_' . $thePerson->ID );
$intro = get_field( 'user_introduction', 'user_' . $thePerson->ID );

$firstName = get_user_meta( $thePerson->ID, 'first_name', TRUE );
$lastName = get_user_meta( $thePerson->ID, 'last_name', TRUE );

$interests = get_field('field_617ef7413834b', 'user_' . $thePerson->ID);
// $birthday = get_field('field_617ef75a3834c', 'user_' . $thePerson->ID);
$gender = get_field('field_617ef78b3834d', 'user_' . $thePerson->ID);


$userTitle = !empty( $trailName ) ? $trailName : $firstName . ' ' . $lastName ;

?>
<main class="userProfile">
	<div id="categories-section">
		<div class="container">
			<div class="profile">
				<div class="profile_left">
					<?php

					$imgSrc = get_stylesheet_directory_uri() . '/assets/icons/placeholder_userPhoto.png';

					if ( 
						!empty($photo) &&
						1 == 1 
						// !empty($photo['sizes']) && 
						// !empty($photo['sizes']['profilePhoto'])  
					)
					{
						$imgSrc = $photo['sizes']['profilePhoto'];
					}

					?>
					<img src="<?php echo $imgSrc; ?>" />
				</div>
				<div class="profile_right">
					<h1 class="trailname">
						<?php echo $userTitle; ?>
					</h1>
					<?php

					if ( 
						!empty( $trailName ) && 
						(
							!empty( $firstName ) || 
							!empty( $lastName ) 
						)
					)
					{
						?>
						<div class="name">
							<label>
								Name
							</label>
							<?php echo $firstName; ?> <?php echo $lastName; ?>
						</div>
						<?php
					}

					if ( !empty( $hometown ) )
					{
						?>
						<div class="hometown">
							<label>
								Hometown
							</label>
							<em><?php echo $hometown; ?></em>
						</div>
						<?php
					}

					if ( !empty( $birthday ) )
					{
						?>
						<div class="birthday">
							<em><?php echo $birthday; ?></em>
						</div>
						<?php
					}

					/*
					if ( !empty( $gender ) )
					{
						?>
						<div class="gender">
							<em><?php echo $gender; ?></em>
						</div>
						<?php
					}
					*/

					/*
					if ( !empty( $gender ) )
					{
						?>
						<div class="gender">
							<em><?php echo $gender; ?></em>
						</div>
						<?php
					}
					*/

					if ( !empty( $interests ) )
					{
						?>
						<div class="interests">
							<label>
								Interests
							</label>
							<em><?php echo $interests; ?></em>
						</div>
						<?php
					}

					if ( !empty( $intro ) )
					{
						?>
						<div class="intro">
							<label>
								About <?php echo $userTitle; ?>
							</label>
							<?php echo wpautop( $intro ); ?>
						</div>
						<?php
					}


					?>
					<div>
						Current Score: <?php echo $numOfBracelets; ?>
					</div>
				</div>


			</div>
			<div class="profileActions">
				<?php

				if ( $thePerson == wp_get_current_user() )
				{
					?>
					<header>
						<button id="updateProfile_toggle" uk-toggle="target: #updateProfile_container, #updateProfile_toggle">
							Update My Profile
						</button>
					</header>
					<div id="updateProfile_container" class="updateProfile_container" hidden>
						<form method="post" id="user_update" enctype="multipart/form-data">
							<div class="updateProfileFieldGroup double">
								<input type="text" name="user_firstname" placeholder="First Name" value="<?php echo $firstName; ?>" />
								<input type="text" name="user_lastname" placeholder="Last Name" value="<?php echo $lastName; ?>" />
							</div>
							<div class="updateProfileFieldGroup ">
								<input type="text" name="user_trailname" placeholder="Trail Name" value="<?php echo $trailName; ?>" />
							</div>
							<input type="text" name="user_hometown" placeholder="Home Town" value="<?php echo $hometown; ?>" />
							<div class="updateProfileFieldGroup double">
								<div>
									<label>Gender</label>
									<?php

									$allGenderTypes = iq_getGenderTypes();

									if ( !empty( $allGenderTypes ) )
									{
										?>
										<select name="user_gender">
										<?php

										foreach ( $allGenderTypes as $key => $value )
										{
											$isSelectedClass = '';

											if ( $gender == $key )
											{
												$isSelectedClass = 'selected="selected"';
											}

											?>
											<option value="<?php echo $key; ?>" <?php echo $isSelectedClass; ?> ><?php echo $value; ?></option>
											<?php
										}

										?>
										</select>
										<?php
									}

									?>
								</div>
								<div>
									<label>Replace Profile Pic?</label>
									<input type="file" name="user_photo" accept="image/jpeg, image/png" />
								</div>
							</div>
							<div class="updateProfileFieldGroup">
								<label>Tell us about yourself</label>
								<textarea rows="8" name="user_bio" placeholder="Biography"><?php echo $intro; ?></textarea>
							</div>
							<div>
								<label>Interests</label>
								<input type="text" name="user_interests" placeholder="What are you interested in?" value="<?php echo $interests; ?>" />
							</div>

							<footer>
								<input type="submit" value="Save Updated Profile" />
								<input type="hidden" name="memberupdatenonce" value="<?php echo wp_create_nonce('update_nonce'); ?>" />
							</footer>
						</form>
					</div>
					<script>
						var adminAjaxUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
						var formElem = document.querySelector('#user_update');
						
						if ( formElem )
						{
							formElem.addEventListener(
								'submit',
								function( event ){

									event.preventDefault();

									firstnameVal = formElem['user_firstname'].value;
									lastnameVal = formElem['user_lastname'].value;
									trailnameVal = formElem['user_trailname'].value;
									bioVal = formElem['user_bio'].value;
									interestsVal = formElem['user_interests'].value;
									hometownVal = formElem['user_hometown'].value;
									genderVal = formElem['user_gender'].value;
									nonceVal = formElem['memberupdatenonce'].value;
									
									var data = new FormData();
									data.append('firstname', firstnameVal);
									data.append('lastname', lastnameVal);
									data.append('bio', bioVal);
									data.append('trailname', trailnameVal);
									data.append('hometown', hometownVal);
									data.append('gender', genderVal);
									data.append('interests', interestsVal);
									data.append('photo', formElem['user_photo'].files[0]);
									data.append('memberupdatenonce', nonceVal);
									data.append('action', 'doMemberProfileUpdate');

									try {
										fetch( adminAjaxUrl, {
											method: 'POST',
											body: data
										} )
										.then( ( response ) => {
											response.json().then( (jsonObj) => {
												handleSuccess(jsonObj);
											});
										});
									}
									catch ( err ) {
										console.log(456);
										console.log(err);
									}

								}
							);
						}


						function handleSuccess( jsonObj )
						{
							UIkit.modal.alert('Profile Updated!').then(
								() => {
									window.location.href = '<?php echo get_author_posts_url( get_current_user_id() ); ?>';
								}
							);
						}

						function handleFail( jsonResponse )
						{
							// var jsonObj = JSON.parse( jsonResponse );
							// console.log( jsonObj );
						}
					</script>
					<?php
				}


				?>
			</div>
		</div>
	</div>
</main>
<?php

get_footer();

