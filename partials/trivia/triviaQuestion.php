<!-- questions section start -->
<section id="questions-section">
	<div class="container">
		<div class="col-12 col-sm-10 offset-1">
			<h2>Answer the following question</h2>
			<div class="asked-question">
				<h4 id="question1"></h4>
			</div>
			<div class="answer-options">
				<form id="quizForm">

					<div class="btn-wrapper">
						<button id="answer-submit" type="button" class="btn btn-primary" >Submit Answer</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- questions section end -->