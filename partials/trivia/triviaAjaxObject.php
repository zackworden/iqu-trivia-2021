<?php

global $post;

$quizQuestion = get_field( 'trivia_question' );
$quizAnswerCorrect = get_field( 'trivia_correctAnswer' );
$quizAnswersIncorrect = get_field( 'trivia_incorrectAnswer' );
$quizType = get_field( 'trivia_type' );
$quizAnswersAllFormatted = [];

foreach ( $quizAnswersIncorrect as $thisWrongAnswer )
{
	$quizAnswersAllFormatted[] = $thisWrongAnswer['trivia_incorrectAnswer_badAnswer'];
}

$quizAnswersAllFormatted[] = $quizAnswerCorrect;

shuffle( $quizAnswersAllFormatted );

$postArray = [
	'post_id'			=> $post->ID,
	'question'			=> esc_attr( $quizQuestion ),
	'correct'			=> $quizAnswerCorrect,
	'answers'			=> $quizAnswersAllFormatted,
	'type'				=> get_field( 'trivia_type' ),
];

$postJson = json_encode( $postArray );

?>
<script>
	var quizJson = JSON.parse('<?php echo $postJson; ?>');
</script>