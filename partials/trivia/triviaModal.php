<!-- questions modal start -->
<div class="modal fade" id="questionResult" tabindex="-1" role="dialog" aria-labelledby="questionResultCenterTitle" aria-hidden="true"> 
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="questionResultLongTitle">Congratulations</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h3 class="text-success">You have won a bracelet!</h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- questions modal end -->