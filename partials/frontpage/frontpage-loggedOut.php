  <!-- signup section start -->
  <section id="signup-section" class="sign-section">
    <div class="container">
      <div class="col-sm-10 offset-sm-1">
        <div class="sign-form-wrapper">
        <div class="row">
            <div class="col-12 col-sm-6 sign-form-wrapper-left-col">
              <div class="sign-form-wrapper-left">
                <h1>SIGN UP FREE</h1>
                <h4>Bring out your best version</h4>
                <div>
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/arrow2.png" alt="iq trivia signup img" width="" height=""/>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 sign-form-wrapper-right-col">
              <div class="sign-form-wrapper-right">
                <form action="#" method="POST">
                  
                  <div class="form-group">
                    <label for="username"></label>
                    <input type="text" class="form-control" id="username" required="" name="username" aria-describedby="username" placeholder="Username">
                  </div>
                  <div class="form-group">
                    <label for="name"></label>
                    <input type="text" class="form-control" id="name" required="" name="name" aria-describedby="name" placeholder="Name">
                  </div>
                  <div class="form-group">
                    <label for="email"></label>
                    <input type="email" class="form-control" id="email" required="" name="email" aria-describedby="email" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label for="password"></label>
                    <input type="password" class="form-control" id="password" required="" name="password" placeholder="Password">
                  </div>
                  <button type="submit" class="btn btn-primary" name="submit">sign up</button>
                  <p class="signup-para">Already a member? <a href="<?php echo get_site_url(); ?>/login">Sign In</a></p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- signup section end -->


<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/sign.css">