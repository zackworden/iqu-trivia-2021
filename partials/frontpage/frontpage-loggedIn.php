<section id="categories-section">
	<div class="container">

		<h2>
			Choose a category below
		</h2>
		<?php

		$args = [
			'taxonomy'		=> 'category',
			'parent'		=> 0,
			'hide_empty'	=> TRUE,
		];

		$query = new WP_Term_Query( $args );

		if ( !empty( $query->get_terms() ) )
		{
			?>
			<div class='triviaCategories'>
				<?php

				foreach ( $query->get_terms() as $thisTerm )
				{
					if (
						$thisTerm->name != 'Uncategorized'
					)
					{
						$catIcon = get_field( 'category_icon', 'term_' . $thisTerm->term_id );
						$iconUrl = '';

						if ( empty( $catIcon ) )
						{
							$iconUrl = 'https://placekitten.com/400/300';
						}
						else
						{
							$iconUrl = $catIcon['sizes']['categoryIcon'];
						}


						?>
						<a class="quiz-category-wrapper" href="<?php echo get_term_link( $thisTerm->term_id, 'category' ); ?>" >
							<div class="quiz-category-img">
								<img src="<?php echo $iconUrl; ?>" alt="<?php esc_attr( $thisTerm->name ); ?>" />
								
							</div>
							<div class="quiz-category-title">
								<h4>
									<?php echo $thisTerm->name ; ?>
								</h4>
							</div>
						</a>
						<?php
					}
				}

				?>
			</div>
			<?php
		}

		?>
	</div>
</section>