
	<!-- signup section start -->
	<section id="signup-section" class="sign-section">
		<div class="container">
			<div class="col-sm-10 offset-sm-1">
				<div class="sign-form-wrapper">
					<div class="row">
						<div class="col-12 col-sm-6 sign-form-wrapper-left-col">
							<div class="sign-form-wrapper-left">
								<h1>
									SIGN UP FREE
								</h1>
								<h4>
									Bring out your best version
								</h4>
								<div>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/arrow2.png" alt="iq trivia signup img" />
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 sign-form-wrapper-right-col">
							<div class="sign-form-wrapper-right">
								<form id="user_register" action="<?php echo admin_url('admin-ajax.php'); ?>" method="POST" charset="utf-8">
									
									<div class="form-group">
										<label for="name"></label>
										<input type="text" class="form-control" id="name" required="" name="name" aria-describedby="name" placeholder="Name">
									</div>
									<div class="form-group">
										<label for="email"></label>
										<input type="email" class="form-control" id="email" required="" name="email" aria-describedby="email" placeholder="Email">
									</div>
									<div class="form-group">
										<label for="password"></label>
										<input type="password" class="form-control" id="password" required="" name="password" placeholder="Password">
									</div>

									<button type="submit" class="btn btn-primary" name="submit">sign up</button>


									<input type="hidden" name="register_nonce" value="<?php echo wp_create_nonce('register_nonce'); ?>" />

									<p class="signup-para">
										Already a member? <a href="<?php echo get_home_url(); ?>">Sign In</a>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- signup section end -->

<script>
	var adminAjaxUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var formElem = document.querySelector('#user_register');
	
	if ( formElem )
	{
		formElem.addEventListener(
			'submit',
			function( event ){

				event.preventDefault();

				// TODO - add validation

				nameVal = formElem['name'].value;
				emailVal = formElem['email'].value;
				passVal = formElem['password'].value;
				nonceVal = formElem['register_nonce'].value;

				var postData = {
					'name'     : nameVal,
					'email'     : emailVal,
					'pass'      : passVal,
					'loginNonce'  : nonceVal,
					'action'    : 'doMemberSignup'
				};

				jQuery.ajax(
					{
						'type'    : 'POST',
						'url'   : adminAjaxUrl,
						'data'    : postData,
						'success' : handleSuccess,
						'error'   : handleFail
					}
				);

			}
		);
	}


	function handleSuccess( jsonResponse )
	{
		window.location.href = '<?php echo get_home_url(); ?>';
	}

	function handleFail( jsonResponse )
	{
		var jsonObj = JSON.parse( jsonResponse );
		var modalTitle = document.querySelector('.modal-title');
		modalTitle.innerText = 'Something went wrong.';
		var modalSubTitle = document.querySelector('.modal-body > h3');
		modalSubTitle.innerText = jsonObj['description'];
		$('#registerModal').modal('show');
	}


</script>


<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true"> 
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="questionResultLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h3 class="text-danger"></h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/sign.css">