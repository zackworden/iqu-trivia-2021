
	<!-- signin section start -->
	<section id="signin-section" class="sign-section">
		<div class="container">
			<div class="col-sm-10 offset-sm-1">
				<div class="sign-form-wrapper">
				<div class="row">
						<div class="col-12 col-sm-6 sign-form-wrapper-left-col">
							<div class="sign-form-wrapper-left">
								<h1>
									SIGN IN
								</h1>
								<h4>
									Welcome back
								</h4>
								<div>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/arrow.png" alt="iq trivia sign-form img" />
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 sign-form-wrapper-right-col">

							<div class="sign-form-wrapper-right">
								<form id="user_login" action="<?php echo get_site_url(); ?>" method="POST">
									<div class="form-group">
										<label for="email"></label>
										<input type="email" class="form-control" id="email" name="email" aria-describedby="email" placeholder="Email">
									</div>
									<div class="form-group">
										<label for="password"></label>
										<input type="password" class="form-control" id="password" name="password" placeholder="Password">
									</div>
									<button type="submit" class="btn btn-primary" name="submit">sign in</button>
									<a href="<?php echo wp_lostpassword_url( get_home_url() ); ?>">Forgot Password</a>
									<p class="signin-para">
										New to IQ Trivia? <a href="<?php echo get_site_url(); ?>/signup">Sign Up</a>
									</p>

									<input type="hidden" name="login_nonce" value="<?php echo wp_create_nonce('login_nonce'); ?>" />

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- signin section end -->


<script>
	var adminAjaxUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var formElem = document.querySelector('#user_login');
	
	if ( formElem )
	{
		formElem.addEventListener(
			'submit',
			function( event ){

				event.preventDefault();

				// TODO - add validation

				emailVal = formElem['email'].value;
				passVal = formElem['password'].value;
				nonceVal = formElem['login_nonce'].value;

				var postData = {
					'email'       : emailVal,
					'pass'        : passVal,
					'loginNonce'  : nonceVal,
					'action'      : 'doMemberLogin'
				};

				jQuery.ajax(
					{
						'type'    : 'POST',
						'url'     : adminAjaxUrl,
						'data'    : postData,
						'success' : handleSuccess,
						'error'   : handleFail
					}
				);

			}
		);
	}


	function handleSuccess( jsonResponse )
	{
		window.location.href = '<?php echo get_home_url(); ?>';
	}

	function handleFail( jsonResponse )
	{
		var jsonObj = JSON.parse( jsonResponse );
		var modalTitle = document.querySelector('.modal-title');
		modalTitle.innerText = 'Something went wrong.';
		var modalSubTitle = document.querySelector('.modal-body > h3');
		modalSubTitle.innerText = jsonObj['description'];
		$('#registerModal').modal('show');
	}


</script>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/sign.css">